import axios from "axios"
import { envConfig } from "shared/config"

export const http = axios.create({
    baseURL: `${envConfig.apiUrl}`
})

http.interceptors.request.use(config => {
    if (!config.headers.accept) config.headers.accept = "*/*"
    if (!config.headers["Access-Control-Allow-Origin"]) config.headers["Access-Control-Allow-Origin"] = "*"

    return config
})
