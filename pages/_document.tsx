import { Html, Head, Main, NextScript } from "next/document"

export default function Document() {
    return (
        <Html lang="en">
            <Head>
                <link rel="icon" href="/images/logo.svg" />
                <meta name="description" content="" />
                <meta name="og:title" content="template" />
            </Head>
            <body>
                <Main />
                <NextScript />
            </body>
        </Html>
    )
}
