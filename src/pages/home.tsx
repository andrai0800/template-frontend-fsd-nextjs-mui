import Head from "next/head"
const PageHome = () => {
    return (
        <>
            <Head>
                <title>Домашняя страница</title>
            </Head>
        </>
    )
}

export default PageHome
