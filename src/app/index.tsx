import { CssBaseline, ThemeProvider } from "@mui/material"
import { QueryClientProvider } from "react-query"
import { queryClient } from "app/providers/query-provider"
import { AppProps } from "next/app"
import "./styles/globals.scss"
import { ApolloProvider, ApolloClient, InMemoryCache } from "@apollo/client"
import { theme } from "./providers/theme"

const client = new ApolloClient({
    cache: new InMemoryCache()
})

const App = ({ Component, pageProps }: AppProps) => {
    return (
        <>
            <QueryClientProvider client={queryClient}>
                <ApolloProvider client={client}>
                    <ThemeProvider theme={theme}>
                        <CssBaseline />
                        <Component {...pageProps} />
                    </ThemeProvider>
                </ApolloProvider>
            </QueryClientProvider>
        </>
    )
}

export default App
